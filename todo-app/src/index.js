import React from "react"
import ReactDOM from "react-dom"

import TodoContainer from "./components/TodoContainer"

// import Stylesheet
import "./App.css"

// Enabled StrictMode for enables checks and warnings for the component and descentdants

ReactDOM.render(
  <React.StrictMode>
    <TodoContainer />
  </React.StrictMode>,
  document.getElementById("root")
)
