import React from "react";
import styles from "./TodoItem.module.css"

// same as const TodoItem = (props) => {
function TodoItem(props) {
    const completedStyle = {
        fontStyle: "italic",
        color: "#595959",
        opacity: 0.4,
        textDecoration: "line-through",
      }

    const {id, title, completed} = props.todo

    return (
        <li className={styles.item}>
            <input 
                type="checkbox" 
                className={styles.checkbox}
                checked={completed}
                onChange={() => props.handleChangeProps(id)}
            /> 
                <button onClick={() => props.delTodoProps(id)}>
                    Delete
                </button>
                <span style={completed ? completedStyle : null}>
                    {title}
                </span>
        </li>
    )
}

export default TodoItem;