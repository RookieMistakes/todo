import React from "react"
import { v4 as uuidv4 } from "uuid";

import TodosList from "./TodosList"
import InputTodo from "./InputTodo"
import Header from "./Header"

class TodoContainer extends React.Component {
    
    state = {
        todos: [
            {
                id: uuidv4(),
                title: "Setup dev environment",
                completed: true
            },
            {
                id: uuidv4(),
                title: "Develop website and add content",
                completed: false
            },
            {
                id: uuidv4(),
                title: "Deploy to live server",
                completed: false
            }
        ]
    };

    // alternative method wrapped parentisis '()' instead of 'return {}'
    // handleChange = (id) => {
    //     this.setState(prevState => ({
    //         todos: prevState.todos.map(todo => {
    //             if (todo.id === id) {
    //                 return {
    //                     ...todo,
    //                     completed: !todo.completed,
    //                 }
    //             }
    //             return todo;
    //         })
    //     }))
    //   };

    addTodo = (title) => {
        const newTodo = {
            id: uuidv4(),
            title: title,
            completed: false,
        };

        this.setState({
            todos: [...this.state.todos, newTodo]
        })
    };

    delTodo = (id) => {
        this.setState({
            todos: [
                ...this.state.todos.filter(todo => {
                    return todo.id !== id;
                })
            ]
        });
    };

    handleChange = (id) => {
        this.setState(prevState => {
            return {
                todos: prevState.todos.map(todo => {
                    if (todo.id === id) {
                        return {
                            ...todo,
                            completed: !todo.completed,
                        }
                    }
                    return todo;
                })
            }
        });
    };
    
    render() {
        return (
            <div className="container">
                <div className="inner">
                    <Header />
                    <InputTodo 
                        addTodoProps={this.addTodo}/>
                    <TodosList 
                        todos={this.state.todos} 
                        handleChangeProps={this.handleChange}
                        delTodoProps={this.delTodo}/>
                </div>
            </div>
        );
    }
}
export default TodoContainer